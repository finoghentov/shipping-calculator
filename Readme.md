# Shipping Container Calculator

![License](https://img.shields.io/badge/license-MIT-blue.svg)

This project can calculate how many containers of different
sizes you will need to keep different packages

## Software requirements:
- php >= 8.2

## Installation

You can install the library via [Composer](https://getcomposer.org/):

Add dependency in your composer.json:
```json
{
  "require": {
    "pavelf/shipping-calculator": "*"
  }
}
```

And set repository for dependency:
```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "git@gitlab.com:finoghentov/shipping-calculator.git"
    }
  ]
}
```

## Usage

```php
require_once 'vendor/autoload.php';

/**
 * Array of containers with packages inside
 * 
 * @var array<\Pavelf\Container\Container>
 */
$result = (new \Pavelf\Container\Calculator())
    ->pack(new \Pavelf\Container\Package(100, 100, 100))
    ->pack(new \Pavelf\Container\Package(50, 50, 50))
    ->to(new \Pavelf\Container\ContainerType(110, 110, 110))
    ->process();
```

To see more, please look at [example](examples/index.php)

## Advanced Usage

You can define your own strategy of packing if you implement [interface](src/Strategies/StrategyContract.php)
or extend [class](src/Strategies/BaseStrategy.php):

```php
class MyLinearStrategy implements \Pavelf\Container\Strategies\StrategyContract {
    public function pack(array $containerTypes, array $packages): array
    {
        // your logic
    }
}
```

And pass your strategy to calculator
```php
$calculator = new \Pavelf\Container\Calculator(new MyLinearStrategy());

//equals to 

$calculator = new \Pavelf\Container\Calculator();
$calculator->strategy(new MyLinearStrategy());
```

## Testing

For running tests, you should install all dependencies without `--no-dev` flag.

```bash
$ composer install
```

### Psalm
Psalm was used as a static analyzer.

```bash
$ vendor/bin/psalm
```

##### Unit & Feature Testing

Library use PHPUnit as a testing framework.

```bash
$ vendor/bin/phpunit
```

