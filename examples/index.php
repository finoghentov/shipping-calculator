<?php

use Pavelf\Container\Calculator;
use Pavelf\Container\Container;
use Pavelf\Container\ContainerType;
use Pavelf\Container\Package;

require_once __DIR__ . '/../vendor/autoload.php';

$dimensionsTestCases = [
    [
        [78, 93, 79, 79]
    ],
    [
        [30, 90, 60, 24],
        [75, 200, 100, 33]
    ],
    [
        [80, 200, 100, 10],
        [60, 150, 80, 130]
    ]
];

foreach ($dimensionsTestCases as $testCase) {
    $calculator = new Calculator();

    foreach ($testCase as $dimensions) {
        [$width, $length, $height, $amount] = $dimensions;

        for ($i = 0; $i < $amount; $i++) {
            $calculator->pack(new Package($width, $length, $height));
        }
    }

    $result = $calculator
        ->to(new ContainerType(234.8, 1203.1, 234.8))
        ->to(new ContainerType(234.8, 279.4, 234.8))
        ->process();

    printResult($result, $testCase);
}


/**
 * @param array<Container> $containers
 * @param array $testCase
 * @return void
 */
function printResult(array $containers, array $testCase)
{
    say('Following test case:');

    foreach ($testCase as $dimensions) {
        [$width, $length, $height, $amount] = $dimensions;
        say("$amount package(s) with next dimensions: width=$width, length=$length, height=$height");
    }

    foreach ($containers as $container) {
        $innerPackages = [];

        foreach ($container->getPackages() as $package) {
            $key = $package->width . 'x' . $package->height . 'x' . $package->length;

            $innerPackages[$key] = ($innerPackages[$key] ?? 0) + 1;
        }

        say("Container with next dimensions: width={$container->width}, length={$container->length}, height=$container->height. Can hold:");
        foreach ($innerPackages as $key => $amount) {
            say("$amount items of $key");
        }
    }
    say(sprintf("Totally we need at least [%s] container(s)", count($containers)));
    say();
}

function say($string = '')
{
    echo $string . PHP_EOL;
}