<?php

declare(strict_types=1);

namespace Pavelf\Container;

use Pavelf\Container\Exceptions\DuplicatePackage;
use Pavelf\Container\Exceptions\TooBigPackageException;
use Pavelf\Container\Strategies\GuillotineCutHeuristic;
use Pavelf\Container\Strategies\StrategyContract;

/**
 * @psalm-api
 */
class Calculator
{
    /**
     * @psalm-var array<int, ContainerType>
     */
    protected array $containersDimensions = [];

    /**
     * @psalm-var array<string, Package> $containersDimensions
     */
    protected array $packages = [];

    public function __construct(
        protected StrategyContract $strategy = new GuillotineCutHeuristic()
    ) { }

    public function strategy(StrategyContract $strategy): static
    {
        $this->strategy = $strategy;

        return $this;
    }

    /**
     * @throws DuplicatePackage
     */
    public function pack(Package $package): static
    {
        if (isset($this->packages[$package->getId()])) {
            throw new DuplicatePackage();
        }

        $this->packages[$package->getId()] = $package;

        return $this;
    }

    public function to(ContainerType $container): static
    {
        $this->containersDimensions[] = $container;

        return $this;
    }

    /**
     * @psalm-return array<string, Container>
     *
     * @throws TooBigPackageException|DuplicatePackage
     */
    public function process(): array
    {
        try {
            return $this->strategy->pack($this->containersDimensions, $this->packages);
        } finally {
            unset($this->containersDimensions);
            unset($this->packages);
        }
    }
}