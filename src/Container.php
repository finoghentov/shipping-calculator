<?php

declare(strict_types=1);

namespace Pavelf\Container;

use Pavelf\Container\Exceptions\DuplicatePackage;
use Pavelf\Container\Exceptions\TooBigPackageException;
use Pavelf\Container\Figures\Parallelepiped;

class Container extends Parallelepiped
{
    use HasUUID;

    /**
     * @var array<string, Package>
     */
    protected array $packages = [];

    /**
     * @throws DuplicatePackage
     * @throws TooBigPackageException
     */
    public function addPackage(Package $package): void
    {
        if ($package->getVolume() > $this->getVolume()) {
            throw new TooBigPackageException();
        }

        if (isset($this->packages[$package->getId()])) {
            throw new DuplicatePackage();
        }

        $this->packages[$package->getId()] = $package;
    }

    /**
     * @psalm-return  array<string, Package>
     */
    public function getPackages(): array
    {
        return $this->packages;
    }
}