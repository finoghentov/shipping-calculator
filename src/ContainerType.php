<?php

declare(strict_types=1);

namespace Pavelf\Container;

use Pavelf\Container\Figures\Parallelepiped;

class ContainerType extends Parallelepiped
{
    public function makeContainer(): Container
    {
        return new Container(
            $this->width,
            $this->length,
            $this->height
        );
    }
}