<?php

declare(strict_types=1);

namespace Pavelf\Container\Figures;

use Pavelf\Container\Exceptions\IncorrectDimensionsException;

abstract class Parallelepiped
{
    /**
     * @psalm-param int|float $width
     * @psalm-param int|float $length
     * @psalm-param int|float $height
     *
     * @throws IncorrectDimensionsException
     */
    public function __construct(
        readonly public int|float $width,
        readonly public int|float $length,
        readonly public int|float $height,
    ) {
        if (
            $this->width  <= 0 ||
            $this->length <= 0 ||
            $this->height <= 0
        ) {
            throw new IncorrectDimensionsException();
        }
    }

    public function getVolume(): float
    {
        return $this->width * $this->length * $this->height;
    }
}