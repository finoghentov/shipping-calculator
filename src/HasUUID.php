<?php

declare(strict_types=1);

namespace Pavelf\Container;

/**
 * Trait to assign some abstract id to objects
 */
trait HasUUID
{
    /**
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected string $uuid;

    /**
     * @psalm-api
     */
    public function setId(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getId(): string
    {
        /**
         * @psalm-suppress RedundantPropertyInitializationCheck
         */
        if (!isset($this->uuid)) {
            return $this->uuid = uniqid();
        }

        return $this->uuid;
    }
}