<?php

declare(strict_types=1);

namespace Pavelf\Container;

use Pavelf\Container\Figures\Parallelepiped;

class Package extends Parallelepiped
{
    use HasUUID;
}