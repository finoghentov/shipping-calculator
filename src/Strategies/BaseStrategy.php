<?php

declare(strict_types=1);

namespace Pavelf\Container\Strategies;

use Pavelf\Container\Container;
use Pavelf\Container\ContainerType;
use Pavelf\Container\Exceptions\DuplicatePackage;
use Pavelf\Container\Exceptions\TooBigPackageException;
use Pavelf\Container\Figures\Parallelepiped;
use Pavelf\Container\Package;

abstract class BaseStrategy implements StrategyContract
{
    /**
     * Available container types that can be created to hold packages
     *
     * @psalm-var array<ContainerType>
     */
    protected array $containerTypes = [];

    /**
     * @psalm-var array<string, Package>
     */
    protected array $uniquePackages = [];

    /**
     * @psalm-var array<string, Container>
     */
    protected array $resultCollection = [];

    final public function __construct() { }

    /**
     * @psalm-param array<ContainerType> $containerTypes
     * @psalm-param array<string, Package> $packages
     *
     * @psalm-return array<string, Container>
     *
     * @throws TooBigPackageException|DuplicatePackage
     */
    public function pack(array $containerTypes, array $packages): array
    {
        $this->setUniquePackages($packages);
        $this->containerTypes = $this->sortByVolume($containerTypes);

        $this->process($packages);

        $this->optimizeLatestContainer();

        return $this->resultCollection;
    }

    /**
     * Basic strategy to place package inside container
     *
     * Determines does package can fit the empty container
     * Returns first available position that can be packed
     *
     * @psalm-return false|array{int|float, int|float, int|float}
     */
    public function canHold(Container $container, Package $package): false|array
    {
        if ($package->getVolume() > $container->getVolume()) {
            return false;
        }

        $permutations = [
            [$package->width, $package->length, $package->height],
            [$package->width, $package->height, $package->length],
            [$package->length, $package->width, $package->height],
            [$package->length, $package->height, $package->width],
            [$package->height, $package->width, $package->length],
            [$package->height, $package->length, $package->width],
        ];

        foreach ($permutations as $dimensions) {
            if (
                $dimensions[0] <= $container->width &&
                $dimensions[1] <= $container->length &&
                $dimensions[2] <= $container->height
            ) {
                return $dimensions;
            }
        }

        return false;
    }

    /**
     * Checks if we can put all packages from latest container to a smaller one
     *
     * @throws DuplicatePackage
     */
    protected function optimizeLatestContainer(): void
    {
        $resultContainers = $this->resultCollection;

        if (count($resultContainers) < 1 || count($this->containerTypes) <= 1) {
            return;
        }

        $container = array_pop($resultContainers);
        $packages = $container->getPackages();

        foreach (array_reverse($this->containerTypes) as $type) {
            if (
                $type->width !== $container->width &&
                $type->length !== $container->length &&
                $type->height !== $container->height
            ) {
                break;
            }

            try {
                $resultCollection = (new static())->pack([$type], $packages);
            } catch (TooBigPackageException) {
                continue;
            }

            if (count($resultCollection) > 1) {
                continue;
            }

            $resultContainer = array_values($resultCollection)[0];

            unset($this->resultCollection[$container->getId()]);
            $this->resultCollection[$resultContainer->getId()] = $resultContainer;
            break;
        }
    }

    /**
     * @psalm-param array<string, Package> $packages
     */
    protected function setUniquePackages(array $packages): void
    {
        foreach ($packages as $package) {
            $key = $package->height . 'x' . $package->width . 'x' . $package->height;

            if (isset($this->uniquePackages[$key])) {
                continue;
            }

            $this->uniquePackages[$key] = $package;
        }
    }

    /**
     * @template T of Parallelepiped
     * @psalm-param array<T> $items
     * @psalm-param int $sortFlag
     * @psalm-return array<T>
     */
    protected function sortByVolume(array $items, int $sortFlag = SORT_DESC): array
    {
        usort($items, function (Parallelepiped $a, Parallelepiped $b) use ($sortFlag) {
            $condition = $a->getVolume() > $b->getVolume();

            if ($sortFlag === SORT_DESC) {
                $condition = $a->getVolume() < $b->getVolume();
            }

            return $condition ? 1 : -1 ;
        });

        return $items;
    }

    /**
     * @psalm-param array<string, Package> $packages
     *
     * @throws TooBigPackageException
     * @throws DuplicatePackage
     */
    protected abstract function process(array $packages): void;
}