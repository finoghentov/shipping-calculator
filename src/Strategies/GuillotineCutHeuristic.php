<?php

declare(strict_types=1);

namespace Pavelf\Container\Strategies;

use Pavelf\Container\Container;
use Pavelf\Container\Exceptions\DuplicatePackage;
use Pavelf\Container\Exceptions\IncorrectDimensionsException;
use Pavelf\Container\Exceptions\TooBigPackageException;
use Pavelf\Container\Package;

class GuillotineCutHeuristic extends BaseStrategy
{
    /**
     * @var array<Container>
     */
    protected array $subContainers = [];

    /**
     * Containers mapping associated with sub-containers
     *
     * @var array<string, string>
     */
    protected array $subContainerMapping = [];

    /**
     * @throws DuplicatePackage
     * @throws TooBigPackageException
     */
    public function process(array $packages): void
    {
        $packages = $this->sortByVolume($packages);

        while ($package = array_shift($packages)) {
            $container = $this->getContainerFor($package);
            $container->addPackage($package);
        }

        $this->subContainers = [];
        $this->subContainerMapping = [];
    }

    /**
     * @return Container Very important to return main container (not sub-container)
     *
     * @throws TooBigPackageException
     */
    protected function getContainerFor(Package $package): Container
    {
        /**
         * First check if we can put package inside sub-containers
         * If compatible sub-container was not found create new container
         */
        foreach ($this->subContainers as $container) {
            if ($dimensions = $this->canHold($container, $package)) {
                $this->createSubContainers($container, $dimensions);
                unset($this->subContainers[$container->getId()]);
                return $this->resultCollection[$this->subContainerMapping[$container->getId()]];
            }
        }

        return $this->createNewContainerFor($package);
    }

    /**
     * @psalm-param array{int|float, int|float, int|float} $dimensions
     */
    protected function createSubContainers(Container $container, array $dimensions): void
    {
        $freeSpaceDimensions = [
            'width' => $container->width - $dimensions[0],
            'length' => $container->length - $dimensions[1],
            'height' => $container->height - $dimensions[2],
        ];

        $subContainersVariants = [
            [$freeSpaceDimensions['width'], $container->length, $container->height],
            [$dimensions[0], $freeSpaceDimensions['length'], $container->height],
            [$dimensions[0], $dimensions[1], $freeSpaceDimensions['height']]
        ];

        $subContainers = [];

        foreach ($subContainersVariants as $dimensions) {
            try {
                $subContainers[] = new Container(...$dimensions);
            } catch (IncorrectDimensionsException) {
                // Skipping if there is no space for sub-container skipping
                continue;
            }
        }

        foreach ($subContainers as $subContainer) {
            foreach ($this->uniquePackages as $packageType) {
                if ($this->canHold($subContainer, $packageType)) {
                    $this->subContainers[$subContainer->getId()] = $subContainer;
                    $this->subContainerMapping[$subContainer->getId()] =
                        $this->subContainerMapping[$container->getId()] ?? $container->getId();
                }
            }
        }
    }

    /**
     * @param Package $package
     * @return Container
     * @throws TooBigPackageException
     */
    protected function createNewContainerFor(Package $package): Container
    {
        foreach ($this->containerTypes as $type) {
            $container = $type->makeContainer();

            if ($dimensions = $this->canHold($container, $package)) {
                $this->createSubContainers($container, $dimensions);
                $this->resultCollection[$container->getId()] = $container;
                return $container;
            }
        }

        throw new TooBigPackageException();
    }
}