<?php

namespace Pavelf\Container\Strategies;

use Pavelf\Container\Container;
use Pavelf\Container\ContainerType;
use Pavelf\Container\Exceptions\TooBigPackageException;
use Pavelf\Container\Package;

interface StrategyContract
{
    /**
     * @psalm-param array<int, ContainerType> $containerTypes
     * @psalm-param array<string, Package> $packages
     *
     * @psalm-return  array<string, Container>
     *
     * @throws TooBigPackageException
     */
    public function pack(array $containerTypes, array $packages): array;
}