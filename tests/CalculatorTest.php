<?php

declare(strict_types=1);

namespace Tests;

use Pavelf\Container\ContainerType;
use Pavelf\Container\Exceptions\DuplicatePackage;
use Pavelf\Container\Package;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Calculator;
use Tests\Mocks\TestStrategy;

class CalculatorTest extends TestCase
{
    public function testCalculatorCanBeBuildWithProperties(): void
    {
        $packages = [
            new Package(100, 100, 100),
            new Package(50.1, 50.1, 50.1)
        ];

        $containerTypes = [
            new ContainerType(1000, 1000, 1000),
            new ContainerType(2000, 2000, 2000)
        ];

        $strategy = new TestStrategy();

        $calculator = new Calculator();

        $calculator->strategy($strategy);

        foreach ($packages as $package) {
            $calculator->pack($package);
        }

        foreach ($containerTypes as $containerType) {
            $calculator->to($containerType);
        }

        foreach ($calculator->getPackages() as $package) {
            $assertPackage = array_shift($packages);

            $this->assertSame($assertPackage, $package);
        }

        foreach ($calculator->getContainers() as $container) {
            $assertContainer = array_shift($containerTypes);

            $this->assertSame($assertContainer, $container);
        }

        $this->assertSame($strategy, $calculator->getStrategy());
    }

    public function testCalculatorCannotPackTheSamePackageTwice()
    {
        $this->expectException(DuplicatePackage::class);
        $package = new Package(100, 100, 100);

        $calculator = new Calculator();

        $calculator->pack($package);
        $calculator->pack($package);
    }

    public function testCalculatorCanUseStrategyToPackObjects()
    {
        $calculator = new Calculator(new TestStrategy());

        $packages = [
            new Package(100, 100, 100),
            new Package(50.1, 50.1, 50.1)
        ];

        $containerTypes = [
            new ContainerType(1000, 1000, 1000)
        ];

        foreach ($packages as $package) {
            $calculator->pack($package);
        }

        foreach ($containerTypes as $containerType) {
            $calculator->to($containerType);
        }

        $result = $calculator->process();

        foreach ($result as $container) {
            $assertContainerType = array_shift($containerTypes);

            $this->assertEquals([
                $assertContainerType->width,
                $assertContainerType->length,
                $assertContainerType->height
            ], [
                $container->width,
                $container->length,
                $container->height
            ]);

            foreach ($container->getPackages() as $package) {
                $assertPackage = array_shift($packages);

                $this->assertSame($assertPackage, $package);
            }
        }
    }
}