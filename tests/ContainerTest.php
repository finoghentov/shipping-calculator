<?php

declare(strict_types=1);

namespace Tests;

use Pavelf\Container\Container;
use Pavelf\Container\ContainerType;
use Pavelf\Container\Exceptions\DuplicatePackage;
use Pavelf\Container\Exceptions\IncorrectDimensionsException;
use Pavelf\Container\Exceptions\TooBigPackageException;
use Pavelf\Container\Package;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    public function testContainerCannotBeCreatedWithIncorrectDimensions()
    {
        $this->expectException(IncorrectDimensionsException::class);

        new Container(0, 0, 0);
    }

    public function testContainerTypeCannotBeCreatedWithInvalidDimensions()
    {
        $this->expectException(IncorrectDimensionsException::class);

        new ContainerType(0, 0, 0);
    }

    public function testContainerCannotAddDuplicatedPackages(): void
    {
        $this->expectException(DuplicatePackage::class);
        $package = new Package(10, 10, 10);
        $container = new Container(50.5, 50.3, 50.1);
        $container->addPackage($package);
        $container->addPackage($package);
    }

    public function testContainerCanBeCreatedFromType()
    {
        $width = 100;
        $length = 100;
        $height = 100;

        $container = (new ContainerType($width, $length, $height))
            ->makeContainer();

        $this->assertEquals([$width, $length, $height], [$container->width, $container->length, $container->height]);
    }

    public function testContainerCannotHoldPackageBiggerThanItself()
    {
        $this->expectException(TooBigPackageException::class);

        $package = new Package(100, 100, 100);
        $container = new Container(10, 10, 10);
        $container->addPackage($package);
    }

    public function testContainerHasUniqueID()
    {
        $containerA = new Container(100, 100, 100);
        $containerB = new Container(100, 100, 100);

        $this->assertNotEquals($containerA->getId(), $containerB->getId());
    }
}