<?php

namespace Tests\Mocks;

class Calculator extends \Pavelf\Container\Calculator
{
    public function getStrategy()
    {
        return $this->strategy;
    }

    public function getPackages()
    {
        return $this->packages;
    }

    public function getContainers()
    {
        return $this->containersDimensions;
    }
}