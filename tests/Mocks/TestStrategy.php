<?php

namespace Tests\Mocks;

use Pavelf\Container\Strategies\StrategyContract;

class TestStrategy implements StrategyContract
{
    public function pack(array $containerTypes, array $packages): array
    {
        $type = $containerTypes[0];
        $container = $type->makeContainer();

        foreach ($packages as $package) {
            $container->addPackage($package);
        }

        return [$container];
    }
}