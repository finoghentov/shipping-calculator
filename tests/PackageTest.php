<?php

declare(strict_types=1);


use Pavelf\Container\Container;
use Pavelf\Container\Exceptions\IncorrectDimensionsException;
use Pavelf\Container\Package;
use PHPUnit\Framework\TestCase;

class PackageTest extends TestCase
{
    public function testPackageCannotBeCreatedWithIncorrectDimensions()
    {
        $this->expectException(IncorrectDimensionsException::class);

        new Package(0, 0, 0);
    }

    public function testPackageHasUniqueID()
    {
        $containerA = new Container(100, 100, 100);
        $containerB = new Container(100, 100, 100);

        $this->assertNotEquals($containerA->getId(), $containerB->getId());
    }
}